export const ALBUMS = ['Travel', 'Personal', 'Food', 'Nature', 'Other']
export const LIMIT_OPTIONS = [5, 10, 25, 50, 100, 250, 500]
