import React, { useState } from 'react'
import { connect } from 'react-redux'
import { toggleSelected } from '../../_redux/actions'
import PropTypes from 'prop-types'
import './photo.css'

function Photo ({ imageUrl, name, album, toggleSelected, selected, disabled }) {
  const [imageOrientation, setImageOrientation] = useState('')
  function handleImageLoad (e) {
    const imgWidth = e.target.naturalWidth
    const imgHeight = e.target.naturalHeight
    if (imgWidth > imgHeight) {
      setImageOrientation('landscape')
    } else {
      setImageOrientation('portrait')
    }
  }

  function handleClick () {
    !disabled && toggleSelected(name, album)
  }

  return (
    <div className={`photo ${imageOrientation} ${selected ? 'selected' : ''}`} onClick={handleClick}>
      <div className='image-container'>
        <img src={imageUrl} alt={name} onLoad={handleImageLoad} />
      </div>
      <h5>{name}</h5>
      <span>{album}</span>
      <span className='check-mark' />
    </div>
  )
}

Photo.propTypes = {
  imageUrl: PropTypes.string,
  name: PropTypes.string.isRequired,
  album: PropTypes.string.isRequired,
  toggleSelected: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
  disabled: PropTypes.bool
}

export default connect((state, { name, album }) => ({
  selected: state.selected.some(photo => photo.documents === name && photo.album === album)
}), { toggleSelected })(Photo)
