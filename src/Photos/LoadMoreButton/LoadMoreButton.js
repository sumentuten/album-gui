import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchPhotos } from '../../_redux/actions'
import './loadmorebutton.css'
import Button from '../../Button'

function LoadMoreButton ({ fetchPhotos, count, limit }) {
  const [loading, setLoading] = useState(false)
  async function handleClick () {
    setLoading(true)
    await fetchPhotos(count, limit, 'add')
    setLoading(false)
  }
  return <Button action={handleClick} className='load-more-button'>{loading ? 'Loading' : 'Load more'}</Button>
}

LoadMoreButton.propTypes = {
  fetchPhotos: PropTypes.func,
  count: PropTypes.number,
  limit: PropTypes.number
}

export default connect(state => ({
  limit: state.limit,
  count: state.photos.length
}), { fetchPhotos })(LoadMoreButton)
