import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Photo from './Photo'
import Empty from './Empty'
import LoadMoreButton from './LoadMoreButton'
import './photos.css'

function Photos ({ photos, hasSelected, fetching }) {
  return (
    <>
      <div className={`photos ${hasSelected ? 'show-selected' : ''} ${fetching ? 'fetching' : ''}`}>
        {
          photos && photos.length > 0
            ? photos.map((photo, i) => (<Photo key={photo.id || i} imageUrl={photo.raw} name={photo.name} album={photo.album} disabled={fetching} />))
            : <Empty />
        }

      </div>
      <LoadMoreButton />
    </>
  )
}

Photos.propTypes = {
  photos: PropTypes.arrayOf(PropTypes.object)
}

export default connect((state) => ({
  photos: state.photos,
  hasSelected: state.selected.length,
  fetching: state.fetching
}))(Photos)
