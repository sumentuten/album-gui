import React from 'react'
import Controls from './Controls'
import './header.css'

function Header () {
  return (
    <div className='header'><h1>Photos</h1> <Controls /></div>
  )
}

export default Header
