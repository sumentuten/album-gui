import React, { useState } from 'react'
import DeleteButton from './DeleteButton'
import UploadButton from './UploadButton'
import LimitDropdown from './LimitDropdown'
import UploadForm from './UploadForm'
import './controls.css'

function Controls () {
  const [showForm, setShowForm] = useState(false)

  function toggleShowForm () {
    setShowForm(!showForm)
  }

  return (
    <div className='controls'>
      <DeleteButton />
      <UploadButton handleButtonClick={toggleShowForm} />
      <LimitDropdown />
      {showForm ? <UploadForm setShowForm={setShowForm} /> : null}
    </div>
  )
}

export default Controls
