import React from 'react'
import PropTypes from 'prop-types'
import Preview from './Preview'

function FileList ({ files, removeFile }) {
  return (
    <div className={`file-list ${files.length > 0 ? 'grid' : ''}`}>
      {
        files.length > 0
          ? (files.map((file) => (<Preview key={file.name} file={file} clickHandler={removeFile} />)))
          : (<div className='empty-list'>No files selected...</div>)
      }
    </div>
  )
}

FileList.propTypes = {
  files: PropTypes.arrayOf(PropTypes.object).isRequired,
  removeFile: PropTypes.func
}

export default FileList
