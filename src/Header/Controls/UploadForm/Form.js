import React, { useState } from 'react'
import { connect } from 'react-redux'
import Dropdown from '../Dropdown/Dropdown'
import Dropzone from './Dropzone'
import FileList from './FileList'
import UploadButton from '../UploadButton'
import { ALBUMS } from '../../../constants'
import { uploadPhotos, addPhotos } from '../../../_redux/actions'

function Form ({ uploadPhotos, addPhotos, setShowForm }) {
  const [files, setFiles] = useState([])
  const [album, setAlbum] = useState(null)
  const [uploading, setUploading] = useState(false)

  function addFiles (newFiles) {
    // validate files before adding them to the list
    // add only image files and files not yet selected
    const filteredFiles = [...newFiles].filter((file) => file.type.match(/image\//gm) && !files.some(el => el.name === file.name))
    setFiles([...files, ...filteredFiles])
  }

  function hideForm () {
    setShowForm(false)
  }

  async function uploadFiles () {
    if (files.length < 1 || !album) return
    setUploading(true)
    const result = await uploadPhotos(files, album)
    if (result.message === 'OK') {
      hideForm()
      addPhotos(result.data)
      console.log(result.data)
    }
    setUploading(false)
  }

  function removeFile (filename) {
    const filteredFiles = files.filter(file => file.name !== filename)
    setFiles(filteredFiles)
  }

  return (
    <div className='upload-form'>
      <span onClick={hideForm} className='close-button' />
      <h1>Upload photos</h1>
      <Dropzone addFiles={addFiles} />
      <FileList files={files} removeFile={removeFile} />
      <div className='footer'>
        <Dropdown options={ALBUMS} handleSelect={setAlbum} selected={album} label='Album' disabled={uploading} />
        <UploadButton handleButtonClick={uploadFiles} disabled={files.length < 1 || !album || uploading} label={uploading ? 'Uploading' : 'Upload'} />
      </div>
    </div>
  )
}

export default connect(null, { uploadPhotos, addPhotos })(Form)
