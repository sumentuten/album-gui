import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'

function Preview ({ file, clickHandler }) {
  const [url, setUrl] = useState(null)

  function handleClick () {
    clickHandler(file.name)
  }

  useEffect(() => {
    const reader = new window.FileReader()
    reader.onload = function (e) {
      setUrl(e.target.result)
    }
    reader.readAsDataURL(file)
  }, [file])
  return <span onClick={handleClick} className='preview'>{url ? <img src={url} alt={file.name} /> : null}</span>
}

Preview.propTypes = {
  file: PropTypes.object.isRequired,
  handleClick: PropTypes.func
}

export default Preview
