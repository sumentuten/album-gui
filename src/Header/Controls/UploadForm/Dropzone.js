import React, { useRef, useState } from 'react'
import PropTypes from 'prop-types'

function Dropzone ({ addFiles, disabled }) {
  const [active, setActive] = useState(false)
  const input = useRef(null)
  function handleDragEnter (e) {
    e.preventDefault()
    e.stopPropagation()
    !disabled && setActive(true)
  }

  function handleDrop (e) {
    e.preventDefault()
    e.stopPropagation()
    const dt = e.dataTransfer
    const files = dt.files
    if (!disabled) {
      addFiles(files)
      setActive(false)
    }
  }

  function handleDragOver (e) {
    e.preventDefault()
    e.stopPropagation()
  }

  function handleDragLeave (e) {
    e.preventDefault()
    e.stopPropagation()
    !disabled && setActive(false)
  }

  function handleClick (e) {
    e.preventDefault()
    !disabled && input.current.click()
  }

  function handleChange (e) {
    e.preventDefault()
    !disabled && addFiles(e.target.files)
  }

  return (
    <>
      <div className={`drop-zone ${active ? 'active' : ''}`} onDragEnter={handleDragEnter} onDragOver={handleDragOver} onDrop={handleDrop} onDragLeave={handleDragLeave} onClick={handleClick}>
        Drag 'n' drop some files here, or click to select files
      </div>
      <input ref={input} type='file' accept='image/*' onChange={handleChange} hidden multiple />
    </>
  )
}

Dropzone.propTypes = {
  addFiles: PropTypes.func.isRequired,
  disabled: PropTypes.bool
}

export default Dropzone
