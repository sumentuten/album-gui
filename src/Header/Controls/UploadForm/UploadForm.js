import React from 'react'
import PropTypes from 'prop-types'
import Form from './Form'
import './uploadform.css'

function UploadForm ({ setShowForm }) {
  function handleOverlayClick () {
    setShowForm(false)
  }

  return (
    <div className='upload-wrapper'>
      <div onClick={handleOverlayClick} className='upload-overlay' />
      <Form setShowForm={setShowForm} />
    </div>
  )
}

UploadForm.propTypes = {
  setShowForm: PropTypes.func.isRequired
}

export default UploadForm
