import React from 'react'
import PropTypes from 'prop-types'
import UploadIcon from './UploadIcon'
import Button from '../../../Button'
import './uploadbutton.css'

function UploadButton ({ handleButtonClick, disabled, label }) {
  return <Button disabled={disabled} action={handleButtonClick} className='upload-button'><UploadIcon /> {label}</Button>
}

UploadButton.propTypes = {
  handleButtonClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired
}

UploadButton.defaultProps = {
  disabled: false,
  label: 'Upload'
}

export default UploadButton
