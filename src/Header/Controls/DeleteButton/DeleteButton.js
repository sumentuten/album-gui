import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import DeleteIcon from './DeleteIcon'
import './deletebutton.css'
import { deletePhotos, removePhoto, clearSelected } from '../../../_redux/actions'
import Button from '../../../Button'

function DeleteButton ({ count, deletePhotos, removePhoto, clearSelected, selected }) {
  async function handleClick () {
    const res = await deletePhotos()
    if (res.message === 'OK') {
      removePhoto(selected)
      clearSelected()
    }
  }
  return (
    <>{
      count
        ? <Button action={handleClick} className='delete-button'><DeleteIcon />Delete {count} photo{count > 1 ? 's' : ''}</Button>
        : null
    }
    </>
  )
}

DeleteButton.propTypes = {
  count: PropTypes.number.isRequired,
  deletePhotos: PropTypes.func,
  removePhoto: PropTypes.func,
  clearSelected: PropTypes.func,
  selected: PropTypes.array
}

export default connect((state) => ({
  count: state.selected.length,
  selected: state.selected
}), { deletePhotos, removePhoto, clearSelected })(DeleteButton)
