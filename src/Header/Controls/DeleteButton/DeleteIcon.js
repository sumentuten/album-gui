import React from 'react'

function DeleteIcon () {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' x='0' y='0' viewBox='0 0 32 32'>
      <g data-name='Layer 2'>
        <path d='M3 7h2v20.48A3.53 3.53 0 008.52 31h15A3.53 3.53 0 0027 27.48V7h2a1 1 0 000-2h-7V3a2 2 0 00-1.95-2H12a2 2 0 00-2 2v2H3a1 1 0 000 2zm9-4h8v2h-8zm-2 4h15v20.48A1.52 1.52 0 0123.48 29h-15A1.52 1.52 0 017 27.48V7z' />
        <path d='M12.68 25a1 1 0 001-1V12a1 1 0 00-2 0v12a1 1 0 001 1zM19.32 25a1 1 0 001-1V12a1 1 0 00-2 0v12a1 1 0 001 1z' />
      </g>
    </svg>
  )
}

export default DeleteIcon
