import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Options from './Options'
import './dropdown.css'
import Button from '../../../Button'

function Dropdown ({ options, handleSelect, selected, label }) {
  const [show, setShow] = useState(false)
  function handleClick () {
    setShow(!show)
  }
  return (
    <div className='dropdown-wrapper'>
      <Button action={handleClick} className={`dropdown ${show ? 'active' : ''}`}>{selected || label}</Button>
      {show ? <Options options={options} handleSelect={handleSelect} setShow={setShow} /> : null}
    </div>
  )
}

Dropdown.propTypes = {
  options: PropTypes.array.isRequired,
  handleSelect: PropTypes.func.isRequired,
  selected: PropTypes.any,
  label: PropTypes.string
}

Dropdown.defaultProps = {
  label: 'Select'
}

export default Dropdown
