import React from 'react'
import PropTypes from 'prop-types'

function Options ({ options, handleSelect, setShow }) {
  function handleClick (opt) {
    handleSelect(opt)
    setShow(false)
  }
  return (
    <div className='options'>
      {options.map((opt) => (<button key={opt} onClick={() => handleClick(opt)}>{opt}</button>))}
    </div>
  )
}

Options.propTypes = {
  options: PropTypes.array.isRequired
}

export default Options
