import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Dropdown from './Dropdown/Dropdown'
import { LIMIT_OPTIONS } from '../../constants'
import { fetchPhotos, setLimit } from '../../_redux/actions'

function LimitDropdown ({ fetchPhotos, limit = 25, setLimit = () => { } }) {
  async function handleSelect (selected) {
    if (limit !== selected) {
      setLimit(selected)
      // await fetchPhotos()
    }
  }
  return <Dropdown options={LIMIT_OPTIONS} handleSelect={handleSelect} selected={limit} />
}

LimitDropdown.propTypes = {
  limit: PropTypes.number.isRequired,
  setLimit: PropTypes.func,
  fetchPhotos: PropTypes.func
}

export default connect((state) => ({
  limit: state.limit
}), { setLimit, fetchPhotos })(LimitDropdown)
