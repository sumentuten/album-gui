import React from 'react'
import PropTypes from 'prop-types'
import './button.css'

function Button ({ action, children, className, disabled }) {
  function handleClick (e) {
    e.preventDefault()
    action()
  }
  return <button className={`${className} button`} onClick={handleClick} disabled={disabled}>{children}</button>
}

Button.propTypes = {
  action: PropTypes.func.isRequired,
  children: PropTypes.any,
  className: PropTypes.string,
  disabled: PropTypes.bool
}

Button.defaultProps = {
  disabled: false
}

export default Button
