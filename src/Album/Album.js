import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchPhotos } from '../_redux/actions'
import Header from '../Header'
import Photos from '../Photos'
import './album.css'

function Album ({ fetchPhotos }) {
  // Fetch photos on page load
  useEffect(() => {
    fetchPhotos()
  }, [fetchPhotos])

  return (
    <div className='album-wrapper'>
      <Header />
      <Photos />
    </div>
  )
}

export default connect(
  null,
  { fetchPhotos }
)(Album)
