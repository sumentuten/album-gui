import { TOGGLE_SELECTED, SET_FETCHING_STATUS, SET_LIMIT, CLEAR_SELECTED, ADD_PHOTOS, DELETE_PHOTOS } from './actionTypes'

export function fetchPhotos (skip, limit, mode) {
  return async function (dispatch, getState) {
    try {
      const host = process.env.REACT_APP_API_HOST
      const getPhotosUrl = host + '/photos/list'
      const res = await window.fetch(getPhotosUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ skip: skip || 0, limit: getState().limit })
      })
      dispatch(setFetchingStatus(true))
      const parsed = await res.json()
      dispatch({ type: ADD_PHOTOS, payload: { photos: parsed.documents, mode: mode || 'refresh' } })
      dispatch(setFetchingStatus(false))
      return parsed
    } catch (error) {
      console.log(error)
      return error
    }
  }
}

export function uploadPhotos (files, album) {
  return async function (dispatch, getState) {
    try {
      const host = process.env.REACT_APP_API_HOST
      const pushPhotosUrl = host + '/photos'

      const formData = new window.FormData()
      formData.append('album', album)
      files.forEach(file => formData.append('documents', file))

      const res = await window.fetch(pushPhotosUrl, {
        method: 'PUT',
        body: formData
      })
      return res.json()
    } catch (error) {
      console.log(error)
      return error
    }
  }
}

export const addPhotos = (photos) => ({
  type: ADD_PHOTOS,
  payload: { photos: photos, mode: 'add' }
})

export const toggleSelected = (documents, album) => ({
  type: TOGGLE_SELECTED,
  payload: { documents, album }
})

export const clearSelected = () => ({
  type: CLEAR_SELECTED
})

export const setFetchingStatus = status => ({
  type: SET_FETCHING_STATUS,
  payload: status
})

export const setLimit = limit => ({
  type: SET_LIMIT,
  payload: limit
})

export function deletePhotos () {
  return async function (dispatch, getState) {
    try {
      const host = process.env.REACT_APP_API_HOST
      const deletePhotosUrl = host + '/photos'
      const res = await window.fetch(deletePhotosUrl, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(getState().selected)
      })
      return res.json()
    } catch (error) {
      console.log(error)
      return error
    }
  }
}

export const removePhoto = list => ({
  type: DELETE_PHOTOS,
  payload: list
})
