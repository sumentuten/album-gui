import { SET_FETCHING_STATUS } from '../actionTypes'

const initialState = false

function fetchingReducer (state, action) {
  state = state || initialState
  switch (action.type) {
    case SET_FETCHING_STATUS: {
      return action.payload
    }
    default:
      return state
  }
}

export default fetchingReducer
