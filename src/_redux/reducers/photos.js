import { ADD_PHOTOS, DELETE_PHOTOS } from '../actionTypes'

const initialState = []

function photosReducer (state, action) {
  state = state || initialState
  switch (action.type) {
    case ADD_PHOTOS: {
      const { photos, mode } = action.payload
      if (mode === 'add') {
        const combined = [...state, ...photos]
        // filter out duplicates
        const newState = combined.filter((photo, index, self) => self.findIndex(p => p.album === photo.album && p.name === photo.name) === index)
        return newState
      }
      return [...photos]
    }

    case DELETE_PHOTOS: {
      const toBeDeleted = action.payload
      const equalPhotos = (p, tbd) => p.name === tbd.documents && p.album === tbd.album
      const deletedState = state.filter(photo => toBeDeleted.every(tbd => !equalPhotos(photo, tbd)))
      return [...deletedState]
    }

    default:
      return [...state]
  }
}

export default photosReducer
