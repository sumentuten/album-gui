import { combineReducers } from 'redux'
import photos from './photos'
import selected from './selected'
import fetching from './fetching'
import limit from './limit'

export default combineReducers({ photos, selected, fetching, limit })
