import { TOGGLE_SELECTED, CLEAR_SELECTED } from '../actionTypes'

const initialState = []

function selectedReducer (state, action) {
  state = state || initialState
  switch (action.type) {
    case TOGGLE_SELECTED: {
      const file = action.payload
      const found = state.find(el => el.documents === file.documents && el.album === file.album)
      if (found) {
        state.splice(state.indexOf(found), 1)
        return [...state]
      }
      return [...state, file]
    }
    case CLEAR_SELECTED:
      return initialState
    default:
      return [...state]
  }
}

export default selectedReducer
