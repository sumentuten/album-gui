import { SET_LIMIT } from '../actionTypes'

const initialState = 25

function setLimit (state, action) {
  state = state || initialState
  switch (action.type) {
    case SET_LIMIT: {
      return action.payload
    }
    default:
      return state
  }
}

export default setLimit
